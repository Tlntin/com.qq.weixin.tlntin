### 简要说明

1. arch文件储存打包架构，官方打包的是i386，也就是32位程序。amd64是64位。如果arch填写的是32位，那就用官方deb的容器进行封装，如果是64位，那就用vek生成的64位容器进行封装。
2. 精简了400多M字体，需要安装完deb后自行安装该项目自体(**强烈建议**， 可以解决很多小程序自体乱码问题)。[项目地址](https://gitlab.com/Tlntin/wine-extra-fonts)
3. 精简了c/windows目录的Installer文件夹（大概100多M)
4. version储存了微信的版本号，如果微信官方的版本号与之不同（也就是微信更新了，gitlab就会进行打包程序，前提是我设置好CI/CD的定时任务）
5. 更换了最新微信图标，参考了spark-wechat包。
5. 打包成功后可以去ci/cd下载artifacts文件（点击CI/CD-流水线，找到最新job，点击最右边的按钮），里面会保存deb成品。一般保留2周。
6. [成品地址](https://gitlab.com/Tlntin/com.qq.weixin.tlntin/-/jobs)


### 当前微信版本： 3.9.5

"""
功能，用户获取最新微信版本
"""
import time
import requests
from lxml import etree
from random import random
import os


if __name__ == '__main__':
    url = "https://pc.weixin.qq.com/"
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36"
                      " (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36"
    }
    print("开始检查微信版本")
    for i in range(3):
        try:
            res = requests.get(url, headers=headers)
            if res.status_code == 200:
                html = etree.HTML(res.text)
                version = html.xpath('//*[@id="downloadButton"]/div/span/text()')
                if isinstance(version, list):
                    version = version[0]
                    print("微信官网最新版本为：", version)
                    with open("version", "rt", encoding="utf-8") as f:
                        old_version = f.read()
                    if version != old_version:
                        with open("version", "wt", encoding="utf-8") as f:
                            f.write(version)
                        # 执行项目
                        os.system(f"chmod +x build.sh && ./build.sh {version}")
                    else:
                       print("当前版本已经是最新版微信了，无需更新")
                    break
            else:
                print(res)
            time.sleep(1 + random())
        except Exception as err:
            print(err)

#!/bin/bash
package_name=com.qq.weixin.tlntin

# 设置版本号，为了兼容gitlab-ci，采用传参方式获取微信版本
if [ -n "$1" ]; then
  package_version="$1"
else
  package_version=`cat version`
fi
echo ""
echo "即将打包的微信版本号为：${package_version}"
echo ""
arch=`cat arch`
package_container_name="container_for_tlntin_${arch}"
# 依据底包版本
base_version=3.4.0.38deepin4
# 底包架构
base_arch=i386
# 底包名称
base_name=com.qq.weixin.deepin
base_package_name=${base_name}_${base_version}_${base_arch}.deb
base_container_name="Deepin-WeChat"
#
# 底包来源
base_deb_source="https://com-store-packages.uniontech.com/appstore/pool/appstore/c/$base_name/${base_package_name}"

# 微信安装exe文件
wechat_package_name=WeChatSetup.exe
wechat_web_source="https://dldir1.qq.com/weixin/Windows/${wechat_package_name}"

# 当前文件所在路径
project_dir=$(cd `dirname $0`; pwd)
echo "项目所在路径: ${project_dir}" 

# 下载相关文件
if [ ! -d "build" ]; then
  mkdir build 
fi
cd build
if [ ! -f "$base_package_name" ]; then
  echo "开始下载deepin微信" 
  wget "${base_deb_source}" -O "${base_package_name}"
  echo "Deepin微信下载完成"
fi
if [ ! -f "$wechat_package_name" ]; then
  echo "开始下载PC版微信"
  wget "${wechat_web_source}" -O "${wechat_package_name}"
  echo "PC微信下载完成"
fi

# 解包deb
echo "开始解压debian Deb包"
mkdir -p extract/DEBIAN
dpkg -X "./${base_package_name}" extract/DEBIAN
dpkg -e "./${base_package_name}" extract/DEBIAN

# 修改包路径
cd "$project_dir/build/extract/DEBIAN/opt/apps"
mv "${base_name}" "${package_name}"
cd "$project_dir/build/extract/DEBIAN/opt/apps/${package_name}"
# 修改包信息
sed -i "s/${base_name}/${package_name}/g" info
sed -i "s/${base_version}/${package_version}/g" info
sed -i "s/${base_arch}/${arch}/g" info

# 修改运行文件
cd "$project_dir/build/extract/DEBIAN/opt/apps/${package_name}/files"
sed -i "s/${base_container_name}/${package_container_name}/g" run.sh
sed -i "s/${base_name}/${package_name}/g" run.sh
sed -i "s/${base_version}/${package_version}/g" run.sh

# 修改快捷方式命名
cd "$project_dir/build/extract/DEBIAN/opt/apps/${package_name}/entries/applications/"
mv "${base_name}.desktop" "${package_name}.desktop"
sed -i "s/${base_name}/${package_name}/g" "${package_name}.desktop"

# 修改图标命名

cd "$project_dir/build/extract/DEBIAN/opt/apps/${package_name}/entries/icons/hicolor"
# 清空自带图标
rm -rf ./*
# 复制从spark微信提取的最新微信图标入内
cp -r "$project_dir/icons/hicolor/scalable" ./
mv "scalable/apps/com.qq.weixin.spark.png" "scalable/apps/${package_name}.png"
# for dir1 in `ls`
# do
#   if [ -d "$dir1" ]; then
#     mv "${dir1}/apps/${base_name}.svg" "${dir1}/apps/${package_name}.svg"
#   fi
# done

# 封装容器，对于i386,直接用官方容器测试一下先
cd "$project_dir/build/extract/DEBIAN/opt/apps/${package_name}/files"
wechat_dir="drive_c/Program Files/Tencent/WeChat"
if [ ${arch} == "i386" ]; then
  echo "解压官方容器中"
  7z x files.7z -y -aos -o./files
  echo "删除旧版微信"
  rm -rf "files/${wechat_dir}"
else
  # 对于64位平台，直接用64位容器进行打包
  echo "清理官方容器"
  rm -rf files.7z
  cp "${project_dir}/container/amd64/files.7z" .
  echo "解压自建64位容器中"
  # 此7z文件已经精简了微信，字体了，所以不需要再删除微信了
  7z x files.7z -y -aos -o./files
fi
mkdir -p  "files/${wechat_dir}"
echo "解压最新微信包到目标路径" 
7z x "$project_dir/build/${wechat_package_name}" -y -aos -o"files/${wechat_dir}"
echo "重新压缩files.7z"
pwd
rm files.7z
cd "$project_dir/build/extract/DEBIAN/opt/apps/${package_name}/files/files"
7za a ../files.7z *
cd .. && rm -rf files
md5sum files.7z | awk '{print $1}' > files.md5sum

# 修改debian包信息
cd "$project_dir/build/extract/DEBIAN"
sed -i "s/${base_name}/${package_name}/g" control
sed -i "s/${base_version}/${package_version}/g" control
sed -i "s/${base_arch}/${arch}/g" control
# 删除冲突信息
sed -i "s/deepin.com.wechat/${package_name}/g" control
# 修改md5值
sed -i "s/${base_name}/${package_name}/g" md5sums
echo sed -i "s/${base_name}/${package_name}/g" md5sums
echo 重新生成md5值
cat md5sums | awk '{print $2}' | xargs md5sum > new_md5sums
mv new_md5sums md5sums

# 准备重新打包
echo "准备将deb重新打包"
mv opt ../
cd "$project_dir/build"
if [ ! -d "output" ]; then
  mkdir output
fi
dpkg-deb -b extract output/
output_deb="${package_name}_${package_version}_${arch}.deb"
echo "打包完成，输出路径为 ${project_dir}/build/output/${output_deb}"

# 清空一下解压文件
rm -rf "${project_dir}/build/extract"
